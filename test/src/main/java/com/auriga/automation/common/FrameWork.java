package com.auriga.automation.common;

import java.io.File;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FrameWork {

	public static String parentHandle;

	public static WebDriver InitializeBrowser(String browserType, String appURL) {

		WebDriver Initialdriver;
		switch (browserType) {
		case "chrome":
			Initialdriver = initChromeDriver(appURL);
			break;
		case "firefox":
			Initialdriver = initFirefoxDriver(appURL);
			break;
		case "InternetExplorer":
			Initialdriver = initInternetExplorerDriver(appURL);
			break;
		default:
			throw new InvalidParameterException("Invalid Browser type..!!");
		}

		Initialdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return Initialdriver;
	}

	private static WebDriver initChromeDriver(String appURL) {
		System.out.println("Launching google chrome ...");

		System.setProperty("webdriver.chrome.driver",
				GlobaleVariable.Chromedriverpath + "chromedriver.exe");
		WebDriver chromedriver = new ChromeDriver();
		chromedriver.manage().window().maximize();
		chromedriver.navigate().to(appURL);
		return chromedriver;
	}

	private static WebDriver initInternetExplorerDriver(String appURL) {
		System.out.println("Launching Internet Explorer ...");

		System.setProperty("webdriver.ie.driver", GlobaleVariable.IEdriverpath
				+ "IEDriverServer.exe");
		WebDriver IEdriver = new InternetExplorerDriver();
		IEdriver.manage().window().maximize();
		IEdriver.navigate().to(appURL);

		return IEdriver;
	}

	private static WebDriver initFirefoxDriver(String appURL) {

		System.out.println("Launching Firefox browser..");
		WebDriver Firefoxdriver = new FirefoxDriver();
		Firefoxdriver.manage().window().maximize();

		Firefoxdriver.navigate().to(appURL);
		return Firefoxdriver;
	}

	// Pass by value than it will return with find , help to reduce again again
	// write same statement .
	public static WebElement GetElement(WebDriver driver, By by) {
		return driver.findElement(by);

	}

	// Will select a option after passing visible text of drop down option
	public static void SelectByVisibleText(WebDriver driver, By by, String Text) {
		WaitForDropDown(driver, by);
		WebElement qty_dropdwon = driver.findElement(by);
		Select QTY = new Select(qty_dropdwon);
		QTY.selectByVisibleText(Text);
	}

	// Will select a option after passing drop down option value .
	public static void SelectByValue(WebDriver driver, By by, String Text) {
		WaitForDropDown(driver, by);
		WebElement qty_dropdwon = driver.findElement(by);
		Select QTY = new Select(qty_dropdwon);
		QTY.selectByValue(Text);
	}

	// Will select a option after passing drop down Index value .
	public static void SelectByIndex(WebDriver driver, By by, int Text) {
		WaitForDropDown(driver, by);
		WebElement qty_dropdwon = driver.findElement(by);
		Select QTY = new Select(qty_dropdwon);
		QTY.selectByIndex(Text);
	}

	public static void WaitForDropDown(WebDriver driver, By by) {
		WebDriverWait wait1 = new WebDriverWait(driver, 28);
		wait1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
	}

	// Click on WebElement like button or hyperlink .
	public static void Click(WebDriver driver, By by) {
		try {
			WebDriverWait wait1 = new WebDriverWait(driver, 28);
            wait1.until(ExpectedConditions.elementToBeClickable(by));
			// if (driver.findElement(by).isDisplayed())
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed())
					driver.findElement(by).click();
				else
					Log.error("Error in Clicking");
			else
				Log.error("Element Not Found...");
		} catch (Exception e) {
			e.getStackTrace();

		}
	}

	// push data to text field .
	public static void Type(WebDriver driver, By by, String value) {
		try {
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed() && Var.isEnabled()) {
					driver.findElement(by).clear();
					driver.findElement(by).sendKeys(value);
					Log.info(value + "Typed in text field ");
				}

				else {
					Log.error("This field is disabled -> " + Var);
					// throw new
					// IllegalStateException("Error in Clicking on -> " + by );
				}
			else
				Log.error("This Element -> " + Var + "is Not Found.");
		} catch (Exception e) {
			e.getStackTrace();

		}
	}

	// Will Select Radio button value.
	public static void SelectRaidoButton(WebDriver driver, By by) {
		try {
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed() && Var.isEnabled())
					if (!Var.isSelected())
						driver.findElement(by).click();
					else
						Log.error("This field is disabled -> " + Var);

				else
					Log.error("This Element -> " + Var + "is Not Found.");
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	// Will Select Radio button value.
	public static void SelectCheckBox(WebDriver driver, By by) {
		try {
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed() && Var.isEnabled())
					if (!Var.isSelected())
						driver.findElement(by).click();
					else
						Log.error("This Check Box is disabled -> " + Var);

				else
					Log.error("This Element -> " + Var + "is Not Found.");
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	// Will DeSelect Radio button value.
	public static void DeselectCheckBox(WebDriver driver, By by) {
		try {
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed() && Var.isEnabled())
					if (Var.isSelected())
						driver.findElement(by).click();
					else
						Log.error("This Check Box is disabled -> " + Var);

				else
					Log.error("This Element -> " + Var + "is Not Found.");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// Will wait till the page not load else fail
	public static void WaitForPageToLoad(WebDriver driver, int time) {
		driver.manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
	}

	// Will accept alert box.
	public static void AcceptAlert(WebDriver driver) {
		try {
			driver.switchTo().alert().accept();
			Log.info(" Alert  Accepted .");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// Will Dismiss the alert box .
	public static void DismissAlert(WebDriver driver) {
		try {
			driver.switchTo().alert().dismiss();
			Log.error("Alert Dismissed.");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// Will Get text from WebElement
	public static String GetText(WebDriver driver, By by) {
		try {
			WebElement Var = GetElement(driver, by);
			if (Var != null)
				if (Var.isDisplayed())
					return driver.findElement(by).getText();
				else {
					Log.error("This field is not visible -> " + Var);
					return null;
				}
			else {
				Log.error("This Element -> " + Var + "is Not Found.");
				return null;
			}
		} catch (Exception e) {
			e.getStackTrace();
			return null;

		}

	}

	// Will hover on menus and choose option.
	public static void HoverAndClickOnElement(WebDriver driver,
			By HoverElement, By ClickElement) {
		try {
			WebElement HoverOnElement = GetElement(driver, HoverElement);
			WebElement ClickOnElement = GetElement(driver, ClickElement);
			if (HoverOnElement != null && ClickOnElement != null) {
				if (HoverOnElement.isDisplayed()) {
					Actions action = new Actions(driver);
					action.moveToElement(HoverOnElement).build().perform();
					Click(driver, ClickElement);
				} else
					Log.error("This field is not visible -> " + HoverOnElement);
			} else
				Log.error("These Elements -> " + HoverOnElement + " and "
						+ ClickOnElement + "are Not Found.");
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	public static void ClickAndClickOnElement(WebDriver driver,
			By HoverElement, By ClickElement) {
		try {
			WebElement HoverOnElement = GetElement(driver, HoverElement);
			WebElement ClickOnElement = GetElement(driver, ClickElement);
			if (HoverOnElement != null && ClickOnElement != null) {
				if (HoverOnElement.isDisplayed()) {
					Actions action = new Actions(driver);
					// action.moveToElement(HoverOnElement).build().perform();
					action.click(HoverOnElement).build().perform();
					Click(driver, ClickElement);
				} else
					Log.error("This field is not visible -> " + HoverOnElement);
			} else
				Log.error("These Elements -> " + HoverOnElement + " and "
						+ ClickOnElement + "are Not Found.");
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	// Will navigate to desired URL.
	public static void NavigateTo(WebDriver driver, String URL) {
		try {
			driver.navigate().to(URL);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// Will Match WebElement value with expected and return boolean value.
	public static Boolean verifySuccessMessage(WebDriver driver, By by,
			String message) {
		WebElement msgsuccess = GetElement(driver, by);
		WebDriverWait wait1 = new WebDriverWait(driver, 28);
		WebElement w1 = driver.findElement(by);
		wait1.until(ExpectedConditions.textToBePresentInElement(w1, message));
		if (msgsuccess.getText().matches(message)) {
			Log.info(msgsuccess.getText() + " matched");
			return true;

		} else {
			Log.info(msgsuccess.getText() + "not matched matched");
			return false;
		}
	}

	// Will Match WebElement value with expected and return boolean value.
	public static Boolean verifyErrorMessage(WebDriver driver, By by,
			String message) {
		WebElement msgsuccess = GetElement(driver, by);
		WebDriverWait wait1 = new WebDriverWait(driver, 28);
		WebElement w1 = driver.findElement(by);
		wait1.until(ExpectedConditions.textToBePresentInElement(w1, message));
		if (msgsuccess.getText().matches(message)) {

			return true;

		} else {

			return false;
		}
	}

	// Will Match WebElement value with expected and return boolean value.
	public static Boolean verifInputFieldData(WebDriver driver, By by,
			String elemessage) {

		WebElement element = GetElement(driver, by);

		System.out.println(element.getText() + " passed ");

		if (element.getAttribute("value").matches(elemessage)) {

			System.out.println(element.getText() + " passed ");
			Log.info(element.getText() + " passed ");
			return true;
		} else {
			Log.info(element.getText() + " Not Passed ");
			return false;
		}

	}

	// Will Match Drop Down WebElement value with expected and return boolean
	// value.
	public static Boolean verifyDropDownData(WebDriver driver, By by,
			String element) {
		String actualelement = new Select(driver.findElement(by))
				.getFirstSelectedOption().getText();

		System.out.println(actualelement + " passed ");

		if (actualelement.matches(element)) {

			System.out.println(actualelement + " passed ");
			Log.info(actualelement + " passed ");
			return true;
		} else {
			return false;
		}
	}

	public static void clickOnGroupInputEle(WebDriver driver,
			String locatoraddress, int start_loop_from, int endloopto) {

		int i = start_loop_from;
		if (driver.findElement(By.id(locatoraddress + "0")).isDisplayed()
				|| driver.findElement(By.id(locatoraddress + "0")).isSelected())

		{
			try {

				while (i <= endloopto) {
					Log.info("in loop");
					String a = locatoraddress + i;
					driver.findElement(By.id(a)).click();
					Log.info("doHospitalDepartmentnFacilityPage" + i);
					i++;
				}

			} catch (Exception e) {
				e.getStackTrace();
			}
		}
	}

	public static void verifyGroupInputEle(WebDriver driver,
			String locatoraddress, int start_loop_from, int endloopto) {

		int i = start_loop_from;

		try {

			while (i <= endloopto) {
				// Log.info("in loop");
				String a = locatoraddress + i;
				if (driver.findElement(By.id(a)).getAttribute("checked") != null) {
					Log.info("Pass verifyGroupInputEle " + i);
					i++;
				} else {
					Log.info("Fail  verifyGroupInputEle " + i);
					i++;
				}
			}

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	public void addCookie(WebDriver driver, String cookiename,
			String cookievalue) {

		Cookie name = new Cookie(cookiename, cookievalue);
		driver.manage().addCookie(name);

	}

	public void getAllCookieData(WebDriver driver) {
		Set<Cookie> cookies = driver.manage().getCookies();
		// Using advanced For Loop
		for (Cookie cookie : cookies) {
			System.out.println(cookie.getName() + " " + cookie.getValue());
			// This will delete cookie By Name
			driver.manage().deleteCookieNamed(cookie.getName());
		}
	}

	public void deleteAllCookie(WebDriver driver) {
		driver.manage().deleteAllCookies();
	}

	public void deleteNamedCookie(WebDriver driver, String name) {
		driver.manage().deleteCookieNamed(name);
	}

	// to take the screenshot and work with that.
	public static void getScreenShot(WebDriver driver, String path,
			String FileName) {
		try {

			Log.info("Taking Screenshot");
			EventFiringWebDriver edriver = new EventFiringWebDriver(driver);
			File src = edriver.getScreenshotAs(OutputType.FILE);
			File dest = new File(path + FileName + ".png");
			Log.info("Destination file name " + dest);
			FileUtils.copyFile(src, dest);
		} catch (Exception e) {
			// if fails to take screenshot then this block will execute
			e.getStackTrace();
		}
	}

	// To get the current/parent window handle
	public static void getCurrentWindow(WebDriver driver) {
		try {
			parentHandle = driver.getWindowHandle();
		} catch (Exception e) {
			e.getStackTrace();

		}
	}

	// To switch to new child window
	public static void switchToChildWindow(WebDriver driver) {
		try {
			for (String winHandle : driver.getWindowHandles()) {
				// switch focus of WebDriver to the next found window handle
				// (that's your newly opened window)
				driver.switchTo().window(winHandle);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// To switch back to parent window after closing the child window
	public static void switchToParentWindow(WebDriver driver) {
		try {
			driver.switchTo().window(parentHandle);
		} catch (Exception e) {
			e.getStackTrace();

		}
	}

	// right click on the element
	public static void rightClickElement(WebDriver driver, By by) {
		try {
			WebElement rightClick = GetElement(driver, by);
			// WebElement rightClick = driver.findElement(by);

			if (rightClick.isDisplayed()) {
				Actions action = new Actions(driver);
				action.contextClick(rightClick).sendKeys(Keys.ARROW_DOWN)
						.sendKeys(Keys.ENTER).perform();
			} else
				Log.error("This right click is not visible -> " + rightClick);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	// drag and drop

	public static void dragAndDrop(WebDriver driver, By from, By to)
			throws InterruptedException {
		try {
			WebElement dragElementFrom = GetElement(driver, from);
			WebElement dragElementTo = GetElement(driver, to);
			Actions action = new Actions(driver);

			action.dragAndDrop(dragElementFrom, dragElementTo);
			// To generate alert after drag and drop
			JavascriptExecutor javascript = (JavascriptExecutor) driver;
			javascript.executeScript("alert('Element Is drag and drop');");
			Thread.sleep(5000);
			driver.switchTo().alert().accept();

			// To drag and drop element by 100 pixel offset In horizontal
			// direction X.
			action.dragAndDropBy(dragElementFrom, 100, 0).build().perform();
			// To generate alert after horizontal direction drag and drop.
			javascript
					.executeScript("alert('Element Is drag and drop by 100 pixel offset In horizontal direction.');");
			Thread.sleep(5000);
			driver.switchTo().alert().accept();

			// To drag and drop element by 100 pixel offset In Vertical
			// direction Y.
			action.dragAndDropBy(dragElementFrom, 0, 100).build().perform();
			// To generate alert after Vertical direction drag and drop.
			javascript
					.executeScript("alert('Element Is drag and drop by 100 pixel offset In Vertical direction.');");
			Thread.sleep(5000);
			driver.switchTo().alert().accept();

			// To drag and drop element by -100 pixel offset In horizontal and
			// -100 pixel offset In Vertical direction.
			action.dragAndDropBy(dragElementFrom, -100, -100).build().perform();
			// To generate alert after horizontal and vertical direction drag
			// and drop.
			javascript
					.executeScript("alert('Element Is drag and drop by -100 pixel offset In horizontal and -100 pixel offset In Vertical direction.');");
			Thread.sleep(5000);
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	// Will navigate to desired URL.

	// work on the calendar
	public static void calendar(WebDriver driver, By by, By navigate, By table,
			int date) {
		// present inside the frame. use frame handling.
		driver.switchTo().frame(0);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		// click on the bar after that only calendar will be displayed
		WebElement clickOnBar = driver.findElement(by);
		clickOnBar.click();

		// click on the navigation point to pick the date from next month
		WebElement navigationPoint = driver.findElement(navigate);
		navigationPoint.click();

		// select the calendar table
		WebElement dateWidget = driver.findElement(table);
		List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
		List<WebElement> columns = dateWidget.findElements(By.tagName("td"));

		for (WebElement cell : columns) {
			// Select any date from month and write in place of date
			if (cell.getText().equals(date)) {
				cell.findElement(By.linkText("date")).click();
				break;
			}
		}
	}

	// work on kendu ui calendar
	public static void kenduCalendar(WebDriver driver, By by, By navigate,
			By tab, By rows, By col) {

		// present inside the frame. use frame handling.

		// driver.switchTo().frame(0);
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		// click on the bar after that only calendar will be displayed

		WebElement clickOnBar = GetElement(driver, by);
		clickOnBar.click();

		// click on the navigation point to pick the date from next month

		for (int i = 1; i <= 3; i++) {
			WebElement navigationPoint = GetElement(driver, navigate);
			navigationPoint.click();
		}

		// select the calendar table
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(tab));
		// WebElement table = driver.findElement(tab);
		WebElement table = GetElement(driver, tab);

		List<WebElement> tableRows = table.findElements(rows);
		for (WebElement row : tableRows) {
			List<WebElement> cells = row.findElements(col);

			for (WebElement cell : cells) {
				// Select 18th Date
				if (cell.getText().equals("3")) {
					cell.click();
					break;
				}
				// System.out.println(cell.getText());
			}
		}

	}

	// to work with the file download
	public static FirefoxProfile firefoxprofile() {
		String downloadPath = "";
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("browser.download.folderList", 2);
		firefoxProfile.setPreference(
				"browser.download.manager.showWhenStarting", false);
		firefoxProfile.setPreference("browser.download.dir", downloadPath);
		firefoxProfile
				.setPreference(
						"browser.helperApps.neverAsk.saveToDisk",
						"text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");

		return firefoxProfile;
	}

	// to check whether the file is downloaded
	public static boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))

				return flag = true;
		}

		return flag;
	}

	// to get latest file from the directory

	public static File getLatestFilefromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}

		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;
	}

	// to get the extension of the file downloaded

	public static boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 1; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	// to check the file with pdf extn.
	public static void pdfFile(WebDriver driver, By by) {
		WebElement pdf = GetElement(driver, by);
		pdf.click();
		String getURL = driver.getCurrentUrl();
		System.out.println(getURL);
		if (getURL.contains(".pdf")) {
			Log.info(getURL + " present ");
			return;
		} else {
			Log.error(".pdf is not present");
		}

	}

	// to scroll the bottom of the page
	public static void scrollingToBottomofAPage(WebDriver driver, String URL) {
		driver.navigate().to(URL);
		((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	// to scroll particular element of a page
	public static void scrollingToElementofAPage(WebDriver driver, By by,
			String URL) {
		driver.navigate().to(URL + "");
		WebElement element = GetElement(driver, by);
		// WebElement element =
		// driver.findElement(By.linkText("Import/Export"));
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();", element);

	}

	// to scroll by coordinates of page
	public static void scrollingByCoordinatesofAPage(WebDriver driver,
			String URL) {
		driver.navigate().to(URL + "");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
	}

	// to work with the dimension of the browser
	public static void browserDimension(WebDriver driver) {
		System.out.println(driver.manage().window().getSize());
		Dimension d = new Dimension(900, 550);

		// Resize the current window to the given dimension
		driver.manage().window().setSize(d);
	}

	// get color of the button before and after mouse-over
	public static void getCssValueButtonColor(WebDriver driver, By by) {
		WebElement searchBtn = GetElement(driver, by);
		// WebElement googleSearchBtn = driver.findElement(bySearchButton);
		System.out.println("Color of a button before mouse hover: "
				+ searchBtn.getCssValue("color"));
		Actions action = new Actions(driver);
		action.moveToElement(searchBtn).perform();
		System.out.println("Color of a button after mouse hover : "
				+ searchBtn.getCssValue("color"));
	}

	// get the font-size of the button
	public static void getCssValue_ButtonFontSize(WebDriver driver, By by) {
		WebElement searchBtn = GetElement(driver, by);
		// WebElement googleSearchBtn = driver.findElement(bySearchButton);
		System.out.println("Font Size of a button "
				+ searchBtn.getCssValue("font-size"));
	}
	
	public static void clickOnListElementByName(WebDriver driver, By by,int position)
	{	
		List<WebElement> buttons = driver.findElements(By.name("by"));
        WebElement query_enquirymode = buttons.get(position);
        query_enquirymode.click();
	}
	
	public static void clickOnListElementByClassName(WebDriver driver, By by,int position)
	{
		List<WebElement> buttons = driver.findElements(By.className("by"));
        WebElement query_enquirymode = buttons.get(position);
        query_enquirymode.click();
	}

}
