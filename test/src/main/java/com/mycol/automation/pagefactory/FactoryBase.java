package com.mycol.automation.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class FactoryBase {
       
       protected static WebDriver driver;
       protected WebDriverWait Wait ;
       
       public FactoryBase(WebDriver d)
       {
               if(driver == null){
                       driver = d;
               this.Wait        = new WebDriverWait(driver, 60);
               }                        
       }
}
