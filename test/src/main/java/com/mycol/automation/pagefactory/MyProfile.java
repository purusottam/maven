package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.*;

public class MyProfile {
	protected WebDriver driver;

	private By firstName = By.name("firstname");
	private By lastName = By.name("lastname");
	private By email = By.name("email");
	private By mobile = By.name("mobile");
	private By gender = By.xpath(".//*[@id='account']/div[5]/div/div[2]/input");
	private By dob = By.name("dob");
	private By city = By.id("cityList");
	private By submit = By.xpath(".//*[@id='account']/div[9]/div/button");
	private By pageHeading = By.xpath(".//*[@id='account']/h2");
	private By welcome = By.partialLinkText("Welcome");
	private By myaccount = By.linkText("My Account");
	private By success = By.xpath(".//*[@id='content']/div");
	private By otpmessage = By.xpath(".//*[@id='content']/div[1]");

	public MyProfile(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting MyProfile ... ");
	}

	public void doUpdateProfile() {

		FrameWork
				.Type(driver, firstName, Myprofile.MyProfileData.firstnametext);
		FrameWork.Type(driver, lastName, Myprofile.MyProfileData.lastnametext);
		FrameWork.Type(driver, mobile, Myprofile.MyProfileData.mobiletext);
		FrameWork.Click(driver, gender);
		FrameWork.SelectByVisibleText(driver, city,
				Myprofile.MyProfileData.citytext);
		FrameWork.Click(driver, submit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, otpmessage,
				Myprofile.MyProfileData.otpmessageText));

	}

	public void doUpdatewithoutmobileProfile() {

		FrameWork
				.Type(driver, firstName, Myprofile.MyProfileData.firstnametext);
		FrameWork.Type(driver, lastName, Myprofile.MyProfileData.lastnametext);
		// FrameWork.Type(driver, mobile, Myprofile.MyProfileData.mobiletext);
		FrameWork.Click(driver, gender);
		FrameWork.SelectByVisibleText(driver, city,
				Myprofile.MyProfileData.citytext);
		FrameWork.Click(driver, submit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, success,
				Myprofile.MyProfileData.successMessage));

	}

	public void clearProfile() {

		driver.findElement(firstName).clear();
		driver.findElement(lastName).clear();
		driver.findElement(mobile).clear();

	}

	public void verifyPageHeading() {

		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageHeading,
				Myprofile.MyProfileData.pageHeadingText));

	}

	public void openMyProfile() throws InterruptedException {
		// Thread.sleep(3000);
		FrameWork.Click(driver, welcome);
		FrameWork.Click(driver, myaccount);
	}

	/*
	 * public void test() throws InterruptedException{
	 * 
	 * driver.findElement(By.id("search")).sendKeys("pal hospital");
	 * Thread.sleep(3000); List <WebElement> listItems =
	 * driver.findElements(By.className("tt-dataset-hospitals"));
	 * listItems.get(0).click();
	 * driver.findElement(By.id("searchButton")).click(); }
	 */
}
