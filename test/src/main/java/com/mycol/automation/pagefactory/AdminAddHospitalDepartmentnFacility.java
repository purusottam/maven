package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalDepartmentnFacility {
	protected WebDriver driver;
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By linkdepartment = By.linkText("(D) Department and Facilities");
	String chkdepartment = "Hospital_department_"; //loop 24
	String chkexpertise = "Hospital_specialization_"; //loop 24
	String chkfacilities = "Hospital_facility_"; // 5 
	private By btnsubmit = By.xpath(".//*[@id='hospital-form-4']/div[4]/input");
	private By lblsuccess = By.xpath(".//*[@id='msg-hospital-form-4']");
	
	
	public AdminAddHospitalDepartmentnFacility(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyHospitalDepartmentnFacilityPage() {

		//Log.info(driver.findElement(pageheading).getText());
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,Hospital.departmentnfacilities.pageheadingText));

	}

	public void openHospitalDepartmentnFacilityPage() {

		FrameWork.Click(driver, linkdepartment);
	}
	public void doHospitalDepartmentnFacilityPage() {
		
		Log.info("Adding Department and Facility..");
		
		FrameWork.clickOnGroupInputEle(driver, chkdepartment, 0, 24);
		
		FrameWork.clickOnGroupInputEle(driver, chkexpertise, 0, 24);
	 
		FrameWork.clickOnGroupInputEle(driver, chkfacilities, 0, 5);
		FrameWork.Click(driver, btnsubmit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, lblsuccess, Hospital.departmentnfacilities.successnotification));
		
		
	}
	public void verifyDepartmentnFacilityPage()
	{
		Log.info("Verifying Department and Facility..");
		
		FrameWork.verifyGroupInputEle(driver, chkdepartment, 0, 24);
		
		FrameWork.verifyGroupInputEle(driver, chkexpertise, 0, 24);
	 
		FrameWork.verifyGroupInputEle(driver, chkfacilities, 0, 5);
	}
}
