package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AppointConciergeData;

public class AppointConcierge {
	protected WebDriver driver;
	private By btn_appointconcierge = By.id("appoint_concierge_floating");
	private By lbl_appointconcierge = By.className("alert alert-danger alert-dismissible");
	private By drp_patient = By.name("patient");
	private By drp_admission = By.id("doa-value");
	private By drp_hospital = By.id("hospital");
	private By drp_procudure = By.id("procedure");
	private By btn_submit = By.id("saveAppointConciergeInfo");
	private By success = By.id(".//*[@id='content']/div[1]");
	private By lblerrormessage = By.xpath(".//*[@id='doa_error']");

	public AppointConcierge(WebDriver driver) {
		this.driver = driver;
	}
	
	public void openHospitalPage()
	{
		Log.startTestCase("open Front AppointConciergePopup");
		driver.get(AppointConciergeData.appointconcierge.url);
		
		
	}
	public void openAppointConciergePopup() {
		
		FrameWork.Click(driver, btn_appointconcierge);

	}

	public void verifyAppointConciergePopup() {
		Log.info("Verifying Appoint Concierge popup");
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver,
				lbl_appointconcierge,
				AppointConciergeData.appointconcierge.lbl_appointconciergeText));
	}

	public void doAddAppointConcierge() {
		Log.info("Adding Appoint Concierge...");
		FrameWork.SelectByVisibleText(driver, drp_patient,
				AppointConciergeData.appointconcierge.drp_patientText);
		FrameWork.Type(driver, drp_admission,
				AppointConciergeData.appointconcierge.drp_admissionText);
		FrameWork.SelectByVisibleText(driver, drp_hospital,
				AppointConciergeData.appointconcierge.drp_hospitalText);
		FrameWork.SelectByVisibleText(driver, drp_procudure,
				AppointConciergeData.appointconcierge.drp_procudureText);
		FrameWork.Click(driver, btn_submit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, success,
				AppointConciergeData.appointconcierge.successText));
	}

	public void vefiyMandatoryFields() {
		Log.info("Verifying madatory fields");
		openAppointConciergePopup();
		FrameWork.Click(driver, btn_submit);
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver, lblerrormessage, AppointConciergeData.appointconcierge.lblerrormessage));
	}
	

}
