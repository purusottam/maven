package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminmastercitygroupsPage;

public class AdminMasterCityGroups {

	protected WebDriver driver;

	private By master = By.linkText("Masters");
	private By citygroupmanagerlink = By.linkText("City Group Manager");
	private By addbutton = By.linkText("Add City Group Name");
	private By name = By.id("CityGroup_name");
	private By status = By.id("CityGroup_status");
	private By centr_latitude = By.id("CityGroup_center_lat");
	private By centr_longitude = By.id("CityGroup_center_lng");
	private By successnotification = By.xpath(".//*[@id='content']/ul/li/div");
	private By submit = By.name("yt0");
	private By addpageheading = By.xpath(".//*[@id='content']/h1");
	protected String updaturl = "http://admin.staging.colhealth.org/index.php/cityGroup/update/7";
	private By allblankfielderrormessage = By.xpath(".//*[@id='city-group-form']/div[2]/div");

	
	public AdminMasterCityGroups(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting AdminMasterCityGroups ....");
	}
	
	public Boolean doVerifyCityGroupManagePage() {

		Log.startTestCase("doVerifyCityGroupManagePage");
		if (driver.getTitle().matches(AdminmastercitygroupsPage.AdminmastercitygroupsData.title)) {
			Log.info("Pass doVerifyCityGroupManagePage");
			return true;
		} else {
			Log.error("Error in doVerifyCityGroupManagePage");
			return false;
		}
	}

	public void doOpenManageCityGroupPage() {
		Log.info("Opening doOpenManageCityGroupPage");
		//driver.findElement(master).click();
		FrameWork.Click(driver, master);
		//driver.findElement(citygroupmanagerlink).click();
		FrameWork.Click(driver, citygroupmanagerlink);
	}

	public void doOpenAddCityGroupPage() {
		Log.info("Opening doOpenAddCityGroupPage");
		//driver.findElement(addbutton).click();
		FrameWork.Click(driver, addbutton);
	}

	public Boolean doVerifyAddCityGroupPage() {
		if (driver.getTitle().matches(AdminmastercitygroupsPage.AdminmastercitygroupsData.addtitle)
				&& FrameWork.GetText(driver, addpageheading).matches(
						AdminmastercitygroupsPage.AdminmastercitygroupsData.addpageheadingText)) {
			Log.info("Pass doVerifyAddCityGroupPage");
			return true;
		} else {
			Log.error("Error in doVerifyAddCityGroupPage");
			return false;
		}
	}

	public void doAddCityGroup() throws InterruptedException {
		Log.info("Adding doAddCityGroup");
		//driver.findElement(name).clear();
		//driver.findElement(name).sendKeys(AdminmastercitygroupsPage.AdminmastercitygroupsData.nameText);
		FrameWork.Type(driver, name, AdminmastercitygroupsPage.AdminmastercitygroupsData.nameText);
		
		//new Select(driver.findElement(status))
				//.selectByVisibleText(AdminmastercitygroupsPage.AdminmastercitygroupsData.statusText);
		FrameWork.SelectByVisibleText(driver, status, AdminmastercitygroupsPage.AdminmastercitygroupsData.statusText);
		
		FrameWork.Type(driver, centr_latitude, AdminmastercitygroupsPage.AdminmastercitygroupsData.centr_latitudeText);
		
		FrameWork.Type(driver, centr_longitude, AdminmastercitygroupsPage.AdminmastercitygroupsData.centr_longitudeText);
		
		//driver.findElement(submit).click();
		FrameWork.Click(driver, submit);
		Thread.sleep(3000);
		String successNotification = "City Group "
				+ AdminmastercitygroupsPage.AdminmastercitygroupsData.nameText
				+ " created Successfully";
		FrameWork.verifySuccessMessage(driver, successnotification,	successNotification);

	}

	public void doUpdateCityGroup() throws InterruptedException {
		driver.get(updaturl);
		doAddCityGroup();
		driver.get(updaturl);
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, name,
				AdminmastercitygroupsPage.AdminmastercitygroupsData.nameText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, name,
				AdminmastercitygroupsPage.AdminmastercitygroupsData.statusText));
	}

	public void doBlankAllField() throws InterruptedException {
		doOpenManageCityGroupPage();
		doOpenAddCityGroupPage();
		driver.findElement(name).clear();
		//driver.findElement(submit).click();
		FrameWork.Click(driver, submit);
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver, allblankfielderrormessage,
						AdminmastercitygroupsPage.AdminmastercitygroupsData.allblankfielderrormessageText));
	}

}
