package com.mycol.automation.pagefactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.MyCasesData;

public class MyCases {

	protected WebDriver driver;

	public MyCases(WebDriver driver) {
		this.driver = driver;
	}

	private By welcome = By.xpath("html/body/div[1]/div[1]/nav/ul[2]/li[3]/a");
	private By mycases = By.linkText("My Cases");
	private By pageheading = By
			.xpath(".//*[@id='content']/div[2]/div/div[1]/div[1]");
	private By feedbackbox = By.name("feedback");
	private By btnsubmit = By
			.xpath(".//*[@id='content']/div[2]/div/div[1]/div[2]/div/form/button");
	private By lblfeedback = By
			.xpath(".//*[@id='content']/div[2]/div/div[1]/div[2]/div/ul/li");
	private By rating = By.xpath(".//*[@id='content']/div[2]/div/div[1]/div[2]/div/div/div/div[3]");

	public void doOpenMyCasePage() {
		FrameWork.Click(driver, welcome);
		FrameWork.Click(driver, mycases);
		Log.info("opening My Case pages");
	}

	public void doVerifyPageHeading() {
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,
				MyCasesData.pageheadingText));
	}

	public void doAddFeedback() {
		Log.info("Adding Feedback");
		FrameWork.HoverAndClickOnElement(driver, rating, rating);
		FrameWork.Type(driver, feedbackbox, MyCasesData.feedbackboxText);
		FrameWork.Click(driver, btnsubmit);

	}

	public void verifyAddedFeedback() {
		Boolean flag = false;
		List<WebElement> linkElements = driver
				.findElements(By
						.xpath(".//*[@id='content']/div[2]/div/div[1]/div[2]/div/ul/li"));
		for (WebElement element : linkElements) {
			System.out.println(element.getText());
			if (element.getText().contains(MyCasesData.feedbackboxText)) {
				Log.info("Feedback verified " + element.getText());
				flag = true;
			}
			Assert.assertTrue(flag, "Feedback message is not verified");

		}
	}

}
