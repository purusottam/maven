package com.mycol.automation.factorydata;

import org.openqa.selenium.By;

public class Hospital {

	public static class genral {
		// Genral object Data

		public static String nameText = "Marudhar Hospital";
		public static String address1Text = "khatipura";
		public static String address2Text = "jaipur address 2";
		public static String stateText = "Rajasthan";
		public static String cityText = "jaipur";
		public static String localityText = "Ajmeri gate";
		public static String pincodeText = "302018";
		public static String brandText = "Yatharth Wellness Hospital";
		public static String logoText = "/home/aurigalaptop/Purusottam/sms-hospital-jaipur.jpg";
		public static String emailText = "peter1@mailinator.com";
		public static String licenceText = "22334343";
		public static String establishedText = "1979";
		public static String refurbishmentText = "";
		public static String telephoneText = "2345678908";
		public static String faxText = "011-2345678";
		public static String emergencyText = "2345678907";
		public static String ambulanceText = "2345678910";
		public static String accreditationText = "343434343";
		public static String hospitalaboutText = "This is about hospital";
		public static String pageheadinggenral = "(A) General Details";
		public static String title = "My Col - Admin Hospitals";
		public static String addtitle = "My Col - Create Hospitals";
		public static String msgsuccessText = "Saved successfully";
		private static String errstateText = "State cannot be blank.";
		public static String errnameText = "Name cannot be blank.";
		public static String erraddr1Text = "Address1 cannot be blank.";
		public static String errpincodeText = "Pincode cannot be blank.";
	}

	public static class roomdetails {

		public static String pageheadingText = "(B) Room Details";
		public static String bedcountText = "34";
		public static String suitesingleText = "3";
		public static String suitesharingText = "4";
		public static String deleuxesingleText = "1";
		public static String deleuxesharingText = "5";
		public static String genralsingleText = "6";
		public static String genralsharingText = "7";
		public static String genralwardText = "8";
		public static String bedreservedText = "9";
		public static String btnsubmitText = "19";
		public static String msgsuccessText = "Saved successfully";
	}

	public static class patientdetails {
		public static String pageheadingpatientdetailsText = "(C) Patient Details";
		public static String homefoodallowedText = "Yes";
		public static String dailyvisitorsText = "10";
		public static String allowedvisitorsText = "7";
		public static String ipddailypaitentdailyText = "4";
		public static String ipdinceptionpaitentsinceText = "8";
		public static String opdpaitentdailyText = "5";
		public static String opdinceptionpaitentText = "6";
		public static String avrleangthofstayText = "10";
		public static String occupancyrateText = "10";
		public static String opdtimingfrmText = "00:30";
		public static String opdtimingtoText = "01:00";
		public static String visitinghrsfrmText = "01:00";
		public static String visitinghrstoText = "01:45";
		public static String tieuphospitalText = "new tieup hospital";
		public static String successnotificationText = "Saved successfully";
	}

	public static class departmentnfacilities {

		public static String pageheadingText = "(D) Department and Facilities";
		public static String successnotification = "Saved successfully";
	}

	public static class doctordetails {

		public static String doctorpageheadingText = "(E) Doctor Details";
		public static String searchdoctorText = "monika thappar";
		public static String verifydoctorText = "Dr. Monika Thappar";
		public static String nameText = "jatin malhotra";
		public static String ageText = "23";
		public static String emailText = "jatin@mailinator.com";
		public static String mobileText = "2331223233";
		public static String licencenoText = "23423423";
		public static String imageText = "/home/aurigalaptop/Purusottam/sms-hospital-jaipur.jpg";
		public static String hospitalText = "Aakash Eye &General Hospital";
		public static String opdfrmdateText = "00:45";
		public static String opdtodateText = "01:45";
		public static String frmdateText = "06-04-2015";
		public static String todateText = "07-04-2015";
		public static String designationText = "Consultant";
		public static String credentialText = "goodCreden";
		public static String researchpaperText = "newresearch";
		public static String drpaccreditationText = "MBBS";
		public static String txtaccerditaionText = "new Accreditation";

	}

	public static class photos {
		public static String photopageheadingText = "(F) Photos";
		public static String hospitalintensiveText = "1";
		public static String hospitalgenralText = "2";
		public static String hospitalemergencyText = "3";
		public static String hospitalreceptionText = "4";
		public static String hospitalexteriorText = "5";
		public static String hospitalfoodcourtText = "6";
		public static String hospitalotherText = "7";
		public static String btnphotoText = "/home/aurigalaptop/Purusottam/sms-hospital-jaipur.jpg";

	}

	public static class payments {

		public static String pageheadingText = "(G) Payments";
		public static String cghsacceptenceText = "Yes";
		public static String dedicateinsurancehelpText ="Yes";
		// Charges for procedures
		public static String kneereplacementchargeText = "32";
		public static String livertransplantchargeText = "45";
		public static String openheartsurgerychargeText = "40";
		public static String angioplastychargeText = "34";
		public static String consultancychargeText = "45";

		// Insurance Tie Ups
		public static String successnotification = "Saved successfully";
	}

	public static class miscellaneous {

		public static String pageheadingText = "(H) Miscellaneous";
		public static String disfrmairport = "23";
		public static String twowheelparkingslot = "34";
		public static String fourwheelparkingslot = "22";
		public static String twowheelparkingcharge = "34";
		public static String fourwheelparkcharge = "55";
		public static String drpmetrostationfrm = "Karol Bagh";
		public static String metrostationfrmcount = "35";
		public static String railwaystationfrm = "76";
		public static String railwaystationfrmcount = "90";
		public static String distancefrmbusdiponame = "98";
		public static String distancefrmbusdipokm = "78";
		public static String successnotification = "Saved successfully";
	}

	public static class googlemap {

		public static String pageheading = "(I) Google Map";
		public static String latitude = "26.925800";
		public static String longitude = "75.745991";
		public static String successmsgText = "Saved successfully";

	}

	public static class accommodation {

		public static String pageheadingText = "(J) Accommodation Nearby";
		public static String name = "new accommodation";
		public static String noofbeds = "5";
		public static String indicativetariff = "23";
		public static String contacctpersonname = "sambhu lal ";
		public static String contactpersonno = "9867574646";
		public static String errormobilenoti = "Please enter a valid phone number.";
		public static String errornamenoti = "Name cannot be blank.";
		public static String errorpersonname = "Person Name can't be blank.";

	}

}
