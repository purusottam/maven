package com.mycol.automation.factorydata;

public class AdminMasterDepartmentManagerData {
	
	public static String title = "My Col - Admin Departments";
	public static String addDepartmenttitle = "My Col - Create Departments";
	public static String departmentText = "Physiology, Anatomy & Genetics";
	public static String statusText = "Enable";
	public static String addDepartmentpageheadingText = "Create Department";
	public static String departmentblankfielderrormessageText = "Department cannot be blank.";

}
