package com.mycol.automation.factorydata;

public class Concierge {
	
	public static class ConciergeData
	{
		public static String pageTitle = "My Col - Admin Concierge";
		public static String pageheadingText = "Manage Concierges";

		// Update page title
		public static String updatecontitle = "My Col - Update Concierge";

		// Add con
		public static String conpagelink = "http://admin.staging.colhealth.org/index.php/concierge/admin";
		public static String addconciergePageHeadingText = "Create Concierge";
		public static String firstnameText = "nirmal8";
		public static String lastnameText = "sharma";
		public static String startdateText = "11-03-2015";
		public static String dobText = "07-03-1996";
		public static String mobileText = "1122334495";
		public static String emailText = "niranjan90@mailinator.com";
		public static String localaddressText = "this is local address ";
		public static String localareaText = "this is local area";
		public static String localstateText = "6";
		public static String localcityText = "1";
		public static String localpincodeText = "342422";
		public static String latitudeText = "44.132222";
		public static String longitudeText = "47.23232";
		public static String permanentaddressText = "this is permanent address";
		public static String permanentareaText = "this is permanent area";
		public static String permanentstateText = "6";
		public static String permanentcityText = "1";
		public static String permanentpincodeText = "342123";
		public static String nativeplaceText = "jaipur";
		public static String educationalqualificationText = "MCA , BCA";
		public static String language1Text = "hindi";
		public static String language2Text = "english";

		public static String imageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
		public static String passportimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
		public static String voteridimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
		public static String permanentaddressimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";

	}

}
