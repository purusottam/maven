package com.mycol.automation.factorydata;

public class Myprofile {
	
	public static class MyProfileData
	{
		public static String firstnametext = "naresh";
		public static String lastnametext = "kumar";
		public static String mobiletext = "1111111111";
		public static String dobtext = "10-02-2015";
		public static String citytext = "jaipur"; 
		public static String pageHeadingText = "Profile";
		public static String successMessage = "Profile Updated.";
		public static String otpmessageText = "Please verify OTP send on your mobile.";
	}

}
