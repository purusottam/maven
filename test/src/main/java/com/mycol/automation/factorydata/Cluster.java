package com.mycol.automation.factorydata;

public class Cluster {
	
	public static class ClusterData
	{
		public static String clustermanage = "http://admin.staging.colhealth.org/index.php/cluster/admin";
		public static String clusternameText = "New cluser10";
		public static String clusterdesText = "New cluseter description";
		public static String hospitalnameText = "Kailash Hospitals";
		public static String searchText = "Karol Bagh, New Delhi, Delhi, India";
		public static String title = "My Col - Admin Cluster";
		public static String addclustertitle = "My Col - Create Cluster";
		public static String addclusterpageheadingText = "Create Cluster";
		public static String allblankfielderrormessageText = "Please Select Atleast One Hospital";
		public static String Updatetitle = "My Col - Update Cluster";
	}

}
