package com.mycol.automation.factorydata;

public class AdminMasterCaseStatusData {
	
	public static String title = "My Col - Admin Cases Status";
	public static String addCaseStatustitle = "My Col - Add Cases Status";
	public static String emailTemplateText = "latest template";
	public static String statusText = "Origin1";
	public static String orderText = "order1";
	public static String messageTemplateText = "OTP Send";
	public static String enableText = "Yes";
	public static String addCaseStatuspageheadingText = "Create Cases Status";
	public static String statusBlankfieldErrormessageText = "Status cannot be blank.";
	public static String orderBlankfieldErrormessageText = "Order cannot be blank.";

}
