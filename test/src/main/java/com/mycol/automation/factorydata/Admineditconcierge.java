package com.mycol.automation.factorydata;

public class Admineditconcierge {
	
	public static class AdmineditconciergeData
	{
	public static String pageTitle = "My Col - Admin Concierge";
	public static String pageheadingText = "Manage Concierges";
	public static String firstnameText = "niranjan3";
	public static String lastnameText = "sharma";
	public static String startdateText = "12-03-2015";
	public static String dobText = "08-03-1996";
	public static String mobileText = "4422331122";
	public static String emailText = "niranjan93@mailinator.com";
	public static String localaddressText = "this is local address ";
	public static String localareaText = "this is local area";
	public static String localstateText = "6";
	public static String localcityText = "1";
	public static String localpincodeText = "342422";
	public static String latitudeText = "44.132222";
	public static String longitudeText = "47.23232";
	public static String permanentaddressText = "this is permanent address";
	public static String permanentareaText = "this is permanent area";
	public static String permanentstateText = "6";
	public static String permanentcityText = "1";
	public static String permanentpincodeText = "342123";
	public static String nativeplaceText = "jaipur";
	public static String educationalqualificationText = "MBBS , IT Fitter ";
	public static String language1Text = "tamil";
	public static String language2Text = "telgu";
	public static String imageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
	public static String passportimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
	public static String voteridimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";
	public static String permanentaddressimageLink = "/home/aurigalaptop/Desktop/X1GT2mrr.png";

	}
}
