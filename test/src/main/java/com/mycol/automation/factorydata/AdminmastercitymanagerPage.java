package com.mycol.automation.factorydata;

public class AdminmastercitymanagerPage {
	
	public static class AdminmastercitymanagerData
	{
		public static String title = "My Col - Admin Cities";
		public static String addcitytitle = "My Col - Create Cities";
		public static String nameText = "Jaisalmer";
		public static String grp_nameText = "KMR";
		public static String stateText = "Rajasthan";
		public static String blankStateText = "Select state";
		public static String statusText = "Enable";
		public static String center_latitudeText = "26.9200° N";
		public static String center_longitudeText = "70.9000° E";
		public static String addcitypageheadingText = "Create City";
		public static String updatetitle =  "My Col - Update CityGroup";
		public static String stateBlankfieldErrormessageText = "State cannot be blank.";
		public static String nameBlankfieldErrormessageText = "Name cannot be blank.";
		public static String latitudeBlankfieldErrormessageText = "Center Latitude cannot be blank.";
		public static String longitudeBlankfieldErrormessageText = "Center Longitude cannot be blank.";
		
	}

}
