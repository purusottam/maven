package com.mycol.automation.tests;


import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.*;
public class MyProfileTest extends TestBaseSetup {
	
	private WebDriver driver;

	private SignInPage signInPage;
	private MyProfile myprofile;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		myprofile = new MyProfile(driver);
		signInPage = new SignInPage(driver);
		 
	}
	
	@Test(priority=0,enabled=true)
	public void openMyProfileTest() throws InterruptedException
	{
		signInPage.doLogin();
		myprofile.openMyProfile();
	}
	
	@Test(priority=1,enabled=true)
	public void verifyPageHeadingFuntion()
	{
	myprofile.verifyPageHeading();
	}
	
	@Test(priority=2,enabled=true)
	public void doUpdateProfileTest()
	{
		myprofile.clearProfile();
		myprofile.doUpdateProfile();
	
	}
	@Test(priority=3,enabled=true)
	public void doUpdatewithoutmobileProfileTest()
	{
		myprofile.clearProfile();
		myprofile.doUpdatewithoutmobileProfile();
	
	}
	
	
	
}
