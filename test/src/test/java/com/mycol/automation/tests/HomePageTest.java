package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.HomePage;

public class HomePageTest extends TestBaseSetup{
	
	private WebDriver driver;
	HomePage basePage;
	@BeforeClass
	public void setUp() {
		driver=getDriver();
		basePage = new HomePage(driver);
	}
	
	@Test
	public void verifyHomePage() throws InterruptedException {
		System.out.println("Home page test...");
		basePage.verifyBasePageTitle();
	}

}