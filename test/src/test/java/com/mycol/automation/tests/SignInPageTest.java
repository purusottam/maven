package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.SignInPage;

public class SignInPageTest extends TestBaseSetup {
	private WebDriver driver;
	private SignInPage signInPage;

	@BeforeClass
	public void setup() {
		driver = getDriver();

		signInPage = new SignInPage(driver);

	}
	
	@Test(priority = 0, enabled = true)
	public void verifyPageHeadingandPageTitle() throws InterruptedException {
		
		signInPage.verifyPageHeading();
	}
	
	@Test(priority = 1, enabled = true)
	public void blankUserNameNPasswordTest() {
	
		signInPage.blankUserNameNPassword();
	}
	
	@Test(priority = 2, enabled = true)
	public void validUserNameBlankPasswordTest() {
	
		signInPage.validUserNameBlankPassword();
	}
	@Test(priority = 3, enabled = true)
	public void validPasswordeBlankUername() {
	
		signInPage.validPasswordeBlankUername();
	}
	@Test(priority = 4, enabled = false)
	public void doLoginTest() throws InterruptedException {
		signInPage.doLogin();
	}
	@Test(priority = 5, enabled = false)
	public void doLoginByMobileTest() throws InterruptedException {
		signInPage.doLoginByMobile();
	}
	
	
}
